import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import ApiKeys from './src/ApiKeys';
import * as firebase from 'firebase';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    if (!firebase.apps.length) {
    firebase.initializeApp(ApiKeys.FirebaseConfig);
    }
    
  
  }

  render(){
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <StatusBar style="auto" />
        <Button 
          title="Show"
          onPress={readUserData}
        />
      </View>
    );
  }
  
}

function readUserData() {
  firebase.database().ref('Clients/').on('value', function (snapshot) {
    console.log(snapshot.val())
  });
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
